import {Component, OnInit, ViewChild, ElementRef, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Modeler, OriginalPropertiesProvider, PropertiesPanelModule, InjectionNames, OriginalPaletteProvider} from "./bpmn-js/bpmn-js";
import {CustomPropsProvider} from './props-provider/CustomPropsProvider';
import {CustomPaletteProvider} from "./props-provider/CustomPaletteProvider";

import {minimapModule} from "./bpmn-js/bpmn-js";
import customTranslate from './translate/customTranslate';
import {  } from '@angular/common';

const customModdle = {
  name: "customModdle",
  uri: "http://example.com/custom-moddle",
  prefix: "custom",
  xml: {
    tagAlias: "lowerCase"
  },
  associations: [],
  types: [
    {
      "name": "ExtUserTask",
      "extends": [
        "bpmn:UserTask"
      ],
      "properties": [
        {
          "name": "worklist",
          "isAttr": true,
          "type": "String"
        }
      ]
    },
  ]
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Angular/BPMN';
  modeler;

  @ViewChild('downloadBtn') downloadBtn: ElementRef;

  customTranslateModule = {
    translate: [ 'value', customTranslate]
  }

/*   const customTranslateModule = {
    translate: [ 'value', function customTranslate(template, replacements) {
      replacements = replacements || {};
    
      // Translate
      template = translations[template] || template;
    
      // Replace
      return template.replace(/{([^}]+)}/g, function(_, key) {
        return replacements[key] || '{' + key + '}';
      });
    } ]
  }; */

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.modeler = new Modeler({
      container: '#canvas',
      width: '100%',
      height: '600px',
      additionalModules: [
        
        PropertiesPanelModule,

        // Re-use original bpmn-properties-module, see CustomPropsProvider
        {[InjectionNames.bpmnPropertiesProvider]: ['type', OriginalPropertiesProvider.propertiesProvider[1]]},
        {[InjectionNames.propertiesProvider]: ['type', CustomPropsProvider]},

        // Re-use original palette, see CustomPaletteProvider
        {[InjectionNames.originalPaletteProvider]: ['type', OriginalPaletteProvider]},
        {[InjectionNames.paletteProvider]: ['type', CustomPaletteProvider]},
        minimapModule,
        this.customTranslateModule
      ],
      propertiesPanel: {
        parent: '#properties'
      },
      minimap: {
        parent: "#scminimap"
      },
      moddleExtension: {
        custom: customModdle
      }
    });
  }

  handleError(err: any) {
    if (err) {
      console.warn('Ups, error: ', err);
    }
  }

  load(): void {
    const url = '/assets/bpmn/initial.bpmn';
    this.http.get(url, {
      headers: {observe: 'response'}, responseType: 'text'
    }).subscribe(
      (x: any) => {
        console.log('Fetched XML, now importing: ', x);
        this.modeler.importXML(x, this.handleError);
      },
      this.handleError
    );
  }

  save(): void {
    this.modeler.saveXML((err: any, xml: any) => console.log('Result of saving XML: ', err, xml));
  }

  saveSVG() {
    this.modeler.saveSVG((err, svg) => {
      this.setEncoded(this.downloadBtn.nativeElement, 'diagram.svg', err ? null : svg);
    });
  }

  private setEncoded(link, name, data) {
    var encodedData = encodeURIComponent(data);

    if (data) {
      link.setAttribute('href' , 'data:application/bpmn20-xml;charset=UTF-8,' + encodedData);
      link.setAttribute('download' , name);
    }  
  }

}
